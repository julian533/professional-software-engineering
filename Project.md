# Project

## Task

-   Development of a tiny web service with Python that shows some fictional values of sensors
-   Implementation of a Continous Integration (CI) for the project
-   Deployment of the web service in Kubernetes
-   Implementation of a Continous Deployment (CD)

## Basis of Evaluation

**Documentation**

-   Developer documentation with most important information

**Clean Code**

-   Complience with clean code practices

**Git**

-   Completion of DataCamp course [Introduction of Git](https://learn.datacamp.com/courses/introduction-to-git-for-data-science)
-   Complience with [golden rules of writing commit messages](Git-cheatsheet.md#golden-rules-of-writing-commit-messages)
-   Complience with [Simple Git Flow / GitHub Flow](https://guides.github.com/introduction/flow/)
-   Regularly committing changes

**Python**

-   Test coverage
-   Linting (based on <https://gitlab.com/hs-karlsruhe/ci-templates>)
